package es.uma.web.gui.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.CascadeType;

/**
 * 
 * @author Jose
 * Administrator of Laboratory
 *
 */
@Entity
@Table(name = "ADMINISTRATOR")
public class Administrator implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4995036296341133474L;

	@Id
	@Column (name = "ID_ADMIN")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column (name = "ADMIN_NAME")
	private String loginName;
	
	@Column (name = "PASSWORD")
	private String password;
	
	@Column (name = "NAME")
	private String name;
	
	@Column (name = "LASTNAME")
	private String lastName;
	
	@Column (name = "EMAIL")
	private String email;
	
	//Relacion a laborario OneToOne
	@OneToOne
	@PrimaryKeyJoinColumn
	private Laboratory laboratory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Laboratory getLaboratory() {
		return laboratory;
	}

	public void setLaboratory(Laboratory laboratory) {
		this.laboratory = laboratory;
	}
}
