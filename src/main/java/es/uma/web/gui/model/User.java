package es.uma.web.gui.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Datos de nuevo usuario a registrar
 * @author Jose
 *
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8158296877713206617L;

	@Id
	@Column (name = "ID_USER")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column (name = "LOGIN_NAME")
	private String loginName;
	
	@Column (name = "PASSWORD")
	private String password;
	
	@Column (name = "USER_NAME")
	private String name;
	
	@Column (name = "USER_LASTNAME")
	private String lastName;
	
	@Column (name = "EMAIL")
	private String email;
	
	@ManyToOne()
	@JoinColumn(name = "ID_LAB", nullable = false)
	private Laboratory laboratory;
	
	public String getLoginName() {
		return loginName;
	}
	
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Laboratory getLaboratory() {
		return laboratory;
	}

	public void setLaboratory(Laboratory laboratory) {
		this.laboratory = laboratory;
	}
	
}
