package es.uma.web.gui.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "LABORATORY", uniqueConstraints = { @UniqueConstraint(columnNames = "LAB_NAME") })
public class Laboratory implements Serializable {

	private static final long serialVersionUID = 2040168762535460274L;
	
	@Id
	@Column (name = "ID_LAB")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column (name = "LAB_NAME")
	private String labName;
	
	@OneToOne(mappedBy="laboratory", cascade = CascadeType.ALL)
	private Administrator admin;
	
	@OneToMany(mappedBy = "laboratory", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Set<User> users = new HashSet<User>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabName() {
		return labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public Administrator getAdmin() {
		return admin;
	}

	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
}
