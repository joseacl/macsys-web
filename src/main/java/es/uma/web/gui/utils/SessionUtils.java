package es.uma.web.gui.utils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SessionUtils {
	
	public static final String USER_LOGIN_DATAS = "userlogin";
	public static final String USER_DATA_BASE   = "userInDataBase";
	public static final String DATA_USER        = "dataOfUser";
	public static final String ADMIN_LAB_FLAGS   
	= "adminLab";
	
	public static boolean userLogged() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		if (session.getAttribute(USER_LOGIN_DATAS)!=null) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void saveValueInSession(String key, Object valueToSave) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		session.setAttribute(key, valueToSave);
	}
	
	public static Object getValueInSession(String key) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		return session.getAttribute(key);
	}
	
	public static void logoutOfSession() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		session.invalidate();
	}

}
