package es.uma.web.gui.utils;

import java.util.List;
import java.util.Set;

import org.springframework.ui.ModelMap;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.model.User;
import es.uma.web.gui.service.LaboratoryService;
import es.uma.web.gui.service.LaboratoryServiceImpl;

public class ModelUtils {
	/**
	 * Establece en el objeto Model todos los laboratorios del sistema
	 * @param model
	 */
	public static void putModelListLabs(ModelMap model) {
		//View login (admin lab or user)
		LaboratoryService labService = new LaboratoryServiceImpl();
		
        List<Laboratory> labList = labService.getAllLabs();
        model.put("labsList", labList);
	}
	
	/**
	 * En caso de logeado como Administrador de laboratorio, establece en el objeto model
	 * todos los usuarios de ese laboratorio
	 * @param model
	 */
	public static void putModelListUsers(ModelMap model) {
		LaboratoryService labService = new LaboratoryServiceImpl();
		Boolean isAdmin = (Boolean) SessionUtils.getValueInSession(SessionUtils.ADMIN_LAB_FLAGS);
        if (isAdmin!=null && isAdmin) {
        	DatasLogin adminLogin = (DatasLogin) SessionUtils.getValueInSession(SessionUtils.USER_LOGIN_DATAS);
        	String labName        = adminLogin.getLabName();
	    	
	    	Laboratory laboratory = labService.getLabByName(labName);
	    	Set<User> usersOfLab  = laboratory.getUsers();
	    	if (usersOfLab.isEmpty()) {
	    		model.put("hasUsers", false);
	    	} else {
	    		model.put("hasUsers", true);
	    		model.put("userList", laboratory.getUsers());
	    	}
        }
	}
        
}
