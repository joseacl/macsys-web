package es.uma.web.gui.dao;

import java.util.List;

import es.uma.web.gui.model.Laboratory;

public interface LaboratoryDAO {
	
	public void saveLab(Laboratory lab);
	
	public List<Laboratory> getAllLabs();
	
	public boolean existLabName(String labName);
	
	public Laboratory getLabByName(String labName);
	
	public boolean removeLab(Laboratory lab);

}
