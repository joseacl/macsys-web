package es.uma.web.gui.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.model.User;

public class LaboratoryDAOImpl implements LaboratoryDAO {

	/* Get actual class name to be printed on */
	private static Logger log = Logger.getLogger(UserDAOImpl.class.getName());
	
	private SessionFactory sf;
	
	public LaboratoryDAOImpl(SessionFactory sf) {
		this.sf = sf;
	}
	
	public void saveLab(Laboratory lab) {
		Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        try{
	        session.save(lab);
	        tx.commit();
	        log.info("Laboratorio salvado correctamente");
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace(); 
            
            log.error("Error salvando laboratorio "+e.getMessage());
            
        }finally {
            session.close(); 
        }
		
	}
	
	public List<Laboratory> getAllLabs() {
		Session session = sf.openSession();
		List<Laboratory> labsInSystem = null;
        try{
        	Query query = session.createQuery("from Laboratory");
        	labsInSystem = (List<Laboratory>) query.list();

         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo la lista de laboratorios de la base de datos "+e.getMessage());
         }finally {
            session.close(); 
         }
        return labsInSystem;
	}
	
	public boolean existLabName(String labName) {
		Session session = sf.openSession();
        try{
        	Query query = session.createQuery("from Laboratory where LAB_NAME=:labName");
        	query.setParameter("labName", labName);
        	List<Laboratory> labs = (List<Laboratory>)query.list();
            
            if (labs!=null && labs.size()>0) {
            	return true;
            } 
            return false;
            
         }catch (HibernateException e) {
            e.printStackTrace(); 
         }finally {
            session.close(); 
         }
        return false;
	}

	public Laboratory getLabByName(String labName) {
		Session session = sf.openSession();
        try{
        	Query query = session.createQuery("from Laboratory where LAB_NAME=:labName");
        	query.setParameter("labName", labName);
        	Laboratory lab = (Laboratory)query.uniqueResult();
            
            if (lab!=null) {
            	return lab;
            } 
            return null;
            
         }catch (HibernateException e) {
            e.printStackTrace(); 
         }finally {
            session.close(); 
         }
        return null;
	}
	
	public boolean removeLab(Laboratory lab) {
		boolean result = false;
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
        try{
        	session.delete(lab);
        	tx.commit();            
        	result = true;
         }catch (HibernateException e) {
         	tx.rollback();
            e.printStackTrace(); 
         }finally {
            session.close();
         }
        return result;
	}
}
