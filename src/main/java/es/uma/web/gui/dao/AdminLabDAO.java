package es.uma.web.gui.dao;

import java.util.List;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Administrator;

public interface AdminLabDAO {

	/**
	 * Verifica que el administrador del usuario exista en la bbdd
	 * @param adminToVerify
	 * @return
	 */
	public boolean verifyLogin(DatasLogin adminToVerify);
	
	public Administrator getUserByLoginName(String loginName);
	
	public List<Administrator> getAllAdministrators();
	
}
