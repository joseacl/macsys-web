package es.uma.web.gui.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Administrator;
import es.uma.web.gui.model.Laboratory;

public class AdminLabDAOImpl implements AdminLabDAO {
	/* Get actual class name to be printed on */
	private static Logger log = Logger.getLogger(UserDAOImpl.class.getName());
	
	private SessionFactory sf;
	
	public AdminLabDAOImpl(SessionFactory sf) {
		this.sf = sf;
	}
	
	/**
	 * Comprueba si las crendenciales del usuario son correctas
	 */
	public boolean verifyLogin(DatasLogin adminToVerify) {
		Session session = sf.openSession();
        try{
        	//Select lab
        	String labName = adminToVerify.getLabName();
        	Query query = session.createQuery("from Laboratory where LAB_NAME=:labName");
        	query.setParameter("labName", labName);
        	
            Laboratory lab = (Laboratory)query.uniqueResult();
            
            if (lab!=null) {
            	//Obntemos el dato del administrador
            	Administrator adminOfLab = lab.getAdmin();
            	//Chequeamos los datos
            	String loginName       = adminToVerify.getLoginName();
            	String loginInDb       = adminOfLab.getLoginName();
            	//Se ha encontrado usuario con userId;
                String passwordToLogin = adminToVerify.getPassword();
                String passwordInDB    = adminOfLab.getPassword();

                return loginName.equals(loginInDb) &&
                		passwordToLogin.equals(passwordInDB);
            } 
            return false;
            
         }catch (HibernateException e) {
            e.printStackTrace(); 
         }finally {
            session.close(); 
         }
        return false;
	}
	
	public Administrator getUserByLoginName(String loginName) {
		Session session = sf.openSession();
        try{
        	Query query = session.createQuery("from Administrator where ADMIN_NAME=:loginName");
        	query.setParameter("loginName", loginName);
            Administrator admin = (Administrator)query.uniqueResult();
            
            if (admin!=null) {
            	return admin;
            } 
            return null;
            
         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo administrador de laboratorio de base de datos "+loginName+" "+e.getMessage());
         }finally {
            session.close(); 
         }
        return null;
	}
	
	public List<Administrator> getAllAdministrators() {
		Session session = sf.openSession();
		List<Administrator> adminInSystem = null;
        try{
        	Query query = session.createQuery("from Administrator");
            adminInSystem = (List<Administrator>) query.list();

         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo la lista de administradores de laboratorio de la base de datos "+e.getMessage());
         }finally {
            session.close(); 
         }
        return adminInSystem;
	}
}
