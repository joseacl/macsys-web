package es.uma.web.gui.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import es.uma.web.gui.model.User;

public class UserDAOImpl implements UserDAO {

	/* Get actual class name to be printed on */
	private static Logger log = Logger.getLogger(UserDAOImpl.class.getName());
	
	private SessionFactory sf;
	
	public UserDAOImpl(SessionFactory sf) {
		this.sf = sf;
	}
	
	/**
	 * Comprueba si las crendenciales del usuario son correctas
	 */
	public boolean verifyLogin(User userToCheck) {
		Session session = sf.openSession();
		String loginName = userToCheck.getLoginName();
		String labName   = userToCheck.getLaboratory().getLabName();
        try{
        	Query query = session.createQuery("from User user LEFT JOIN user.laboratory lab "
        			+ "where LOGIN_NAME=:loginName and lab.labName=:labName");
        	query.setParameter("loginName", loginName);
        	query.setParameter("labName", labName);
        	
            Object userJoinLab = (Object)query.uniqueResult();
            
            if (userJoinLab!=null) {
            	Object[] entities = (Object[]) userJoinLab;
            	//Se ha encontrado usuario con userId;
                String passwordToLogin = userToCheck.getPassword();
                //Cogemos la entidad usuario
                User user = (User) entities[0];
                
                String passwordInDB    = user.getPassword();

                return passwordToLogin.equals(passwordInDB);
            } 
            return false;
            
         }catch (HibernateException e) {
            e.printStackTrace(); 
         }finally {
            session.close(); 
         }
        return false;
	}

	/**
	 * Salva el usuario en BBDD
	 */
	public void saveUser(User userToSave) {
		Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        try{
	        session.save(userToSave);
	        tx.commit();
	        log.info("Usuario salvado correctamente");
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace(); 
            
            log.error("Error salvando usuario "+e.getMessage());
            
        }finally {
            session.close(); 
        }
	}
	
	public boolean existUser(User userToCheck) {
		String userName = userToCheck.getLoginName();
    	String labName  = userToCheck.getLaboratory().getLabName();
		Session session = sf.openSession();
        try{
        	Query query = session.createQuery("from User user LEFT JOIN user.laboratory lab "
        			+ "where LOGIN_NAME=:loginName and lab.labName=:labName");
        	query.setParameter("loginName", userName);
        	query.setParameter("labName", labName);
        	//Al hacer el JOIN en la consulta, la query no devuelve un User
            Object userJoinLab = (Object)query.uniqueResult();
            
            if (userJoinLab!=null) {
            	return true;
            } 
            return false;
            
         }catch (HibernateException e) {
            e.printStackTrace();
            String errorMsg = String.format("Error obteniendo usuario %s del laboratorio %s", userName,labName);
            log.error(errorMsg+" "+e.getMessage());
         }finally {
            session.close(); 
         }
        return false;
	}
	
	/**
	 * Obtiene el usuario salvado en la base de datos con nombre loginName
	 */
	public User getUserByLoginName(String loginName) {
		Session session = sf.openSession();
        try{
        	Query query = session.createQuery("from User where LOGIN_NAME=:loginName");
        	query.setParameter("loginName", loginName);
            User user = (User)query.uniqueResult();
            
            if (user!=null) {
            	return user;
            } 
            return null;
            
         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo usuario de base de datos "+loginName+" "+e.getMessage());
         }finally {
            session.close(); 
         }
        return null;
	}
	
	/**
	 * Actualiza un usuario en base de datos
	 */
	public void updateUser(User userToUpdate) {
		Session session = sf.openSession();
        try{
        	session.update(userToUpdate);
         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo usuario de base de datos "+e.getMessage());
         }finally {
            session.close(); 
         }
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		Session session = sf.openSession();
		List<User> userInSystem = null;
        try{
        	Query query = session.createQuery("from User");
            userInSystem = (List<User>) query.list();

         }catch (HibernateException e) {
            e.printStackTrace();
            log.error("Error obteniendo la lista de usuarios de la base de datos "+e.getMessage());
         }finally {
            session.close(); 
         }
        return userInSystem;
	}
	
	public boolean removeUserByUserName(String loginName) {
		boolean result = true;
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
        try{
        	Query query = session.createQuery("delete User where LOGIN_NAME=:loginName");
        	query.setParameter("loginName", loginName);
            int numRows = query.executeUpdate();
            if (numRows > 0) {
            	result = true;
            } else {
            	result = false;
            }
            tx.commit();
            
         }catch (HibernateException e) {
        	if (tx!=null) tx.rollback();
            e.printStackTrace();
            log.error("Error Borrando usuario de base de datos "+loginName+" "+e.getMessage());
            result = false;
         }finally {
            session.close();
            
         }
        return result;
	}
	
	public boolean delete(User userToDelete) {
		boolean result = true;
		Session session = sf.openSession();
		Transaction tx = session.beginTransaction();
        try{
        	session.delete(userToDelete);
            tx.commit();
           
        }catch (HibernateException e) {
        if (tx!=null) tx.rollback();
            e.printStackTrace();
            log.error("Error Borrando usuario de base de datos "+userToDelete.getLoginName()+" "+e.getMessage());
            result = false;
        }finally {
            session.close();
        }
        return result;
	}

}
