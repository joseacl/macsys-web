package es.uma.web.gui.dao;

import java.util.List;
import es.uma.web.gui.model.User;


public interface UserDAO {

	public boolean verifyLogin(User userToCheck);
	
	public void saveUser(User userToSave);
	
	public boolean existUser(User userToCheck);
	
	public User getUserByLoginName(String loginName);
	
	public void updateUser(User userToUpdate);
	
	public List<User> getAllUsers();
	
	public boolean removeUserByUserName(String loginName);
	
	public boolean delete(User userToDelete);
	
}
