package es.uma.web.gui.service;

import java.util.List;
import es.uma.web.gui.model.User;

public interface UserService {
	
	public boolean verifyLogin(User userToCheck);
	
	public void saveNewUser(User userToSave);
	
	public boolean existUser(User userToCheck);
	
	public User getUserByLoginName(String loginName);
	
	public void updateUser(User userToUpdate);
	
	public List<User> getAllUsers();
	
	public boolean removeUserByUserName(String loginName);
	
	public boolean delete(User userToDelete);
}
