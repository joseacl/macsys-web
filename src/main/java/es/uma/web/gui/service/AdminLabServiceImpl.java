package es.uma.web.gui.service;

import java.util.List;

import org.hibernate.SessionFactory;

import es.uma.web.gui.dao.AdminLabDAOImpl;
import es.uma.web.gui.dao.AdminLabDAO;
import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Administrator;
import es.uma.web.gui.utils.HibernateUtils;
import es.uma.web.gui.utils.StringMD;

public class AdminLabServiceImpl implements AdminLabService {

	public boolean verifyLogin(DatasLogin user) {
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		AdminLabDAO adminDao = new AdminLabDAOImpl(sessionFactory);
		//TODO: Cambiar la encriptacion de la password al lado del cliente
		String password = user.getPassword();
		String passwordEncripted = StringMD.getStringMessageDigest(password, StringMD.MD5);
		user.setPassword(passwordEncripted);
		
		return adminDao.verifyLogin(user);
		
	}

	public Administrator getUserByLoginName(String loginName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		AdminLabDAO adminDao = new AdminLabDAOImpl(sessionFactory);
		Administrator admin = adminDao.getUserByLoginName(loginName);
		return admin;
	}
	
	
	public List<Administrator> getAllAdministrators() {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		AdminLabDAO adminDao = new AdminLabDAOImpl(sessionFactory);
		return adminDao.getAllAdministrators();
	}
	

}
