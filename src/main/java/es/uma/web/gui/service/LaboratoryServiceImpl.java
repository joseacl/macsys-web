package es.uma.web.gui.service;

import java.util.List;

import org.hibernate.SessionFactory;

import es.uma.web.gui.dao.LaboratoryDAO;
import es.uma.web.gui.dao.LaboratoryDAOImpl;
import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.utils.HibernateUtils;
import es.uma.web.gui.utils.StringMD;

public class LaboratoryServiceImpl implements LaboratoryService {

	public void saveNewLab(Laboratory lab) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		LaboratoryDAO labDao = new LaboratoryDAOImpl(sessionFactory);
		//Antes de guardar el lab, encriptamos la password del administrador
		String passwordWithEncry = lab.getAdmin().getPassword();
		String passwordEncripted = StringMD.getStringMessageDigest(passwordWithEncry, StringMD.MD5);
		lab.getAdmin().setPassword(passwordEncripted);
		labDao.saveLab(lab);
	}
	
	public List<Laboratory> getAllLabs() {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		LaboratoryDAO labDao = new LaboratoryDAOImpl(sessionFactory);
		return labDao.getAllLabs();
	}

	public boolean existLabName(String labName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		LaboratoryDAO labDao = new LaboratoryDAOImpl(sessionFactory);
		return labDao.existLabName(labName);
	}
	
	public Laboratory getLabByName(String labName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		LaboratoryDAO labDao = new LaboratoryDAOImpl(sessionFactory);
		return labDao.getLabByName(labName);
	}
	
	public boolean removeLab(String labName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		LaboratoryDAO labDao = new LaboratoryDAOImpl(sessionFactory);
		Laboratory labToDelete = labDao.getLabByName(labName);
		return labDao.removeLab(labToDelete);
	}
}
