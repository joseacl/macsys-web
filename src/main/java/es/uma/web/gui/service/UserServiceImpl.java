package es.uma.web.gui.service;

import java.util.List;

import org.hibernate.SessionFactory;

import es.uma.web.gui.dao.UserDAO;
import es.uma.web.gui.dao.UserDAOImpl;
import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.User;
import es.uma.web.gui.utils.HibernateUtils;
import es.uma.web.gui.utils.StringMD;

public class UserServiceImpl implements UserService {

	public boolean verifyLogin(User userToCheck) {
		
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		
		return userDao.verifyLogin(userToCheck);
		
	}

	public void saveNewUser(User userToSave) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		//Antes de salvar el usuario, encriptamos la password
		//TODO: Cambiar la encriptacion de la password al lado del cliente
		String password = userToSave.getPassword();
		String passwordEncripted = StringMD.getStringMessageDigest(password, StringMD.MD5);
		userToSave.setPassword(passwordEncripted);
		userDao.saveUser(userToSave);
	}
	
	public boolean existUser(User userToCheck) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		return userDao.existUser(userToCheck);
	}
	
	public User getUserByLoginName(String loginName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		User user = userDao.getUserByLoginName(loginName);
		return user;
	}
	
	
	public void updateUser(User userToUpdate) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		userDao.updateUser(userToUpdate);
	}
	
	public List<User> getAllUsers() {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		return userDao.getAllUsers();
	}
	
	public boolean removeUserByUserName(String loginName) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		return userDao.removeUserByUserName(loginName);
	}
	
	public boolean delete(User userToDelete) {
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
		//Obtenemos los valores de mapeo de la base de datos
		UserDAO userDao = new UserDAOImpl(sessionFactory);
		return userDao.delete(userToDelete);
	}
	

}
