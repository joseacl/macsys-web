package es.uma.web.gui.service;

import java.util.List;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Administrator;
import es.uma.web.gui.model.User;

public interface AdminLabService {
	
	public boolean verifyLogin(DatasLogin user);
	
	public Administrator getUserByLoginName(String loginName);
	
	public List<Administrator> getAllAdministrators();
	
}
