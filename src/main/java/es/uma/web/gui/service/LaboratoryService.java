package es.uma.web.gui.service;

import java.util.List;

import es.uma.web.gui.model.Laboratory;

public interface LaboratoryService {

	public void saveNewLab(Laboratory lab);
	
	public List<Laboratory> getAllLabs();
	
	public boolean existLabName(String labName);
	
	public Laboratory getLabByName(String labName);
	
	public boolean removeLab(String labName);
}
