package es.uma.web.gui.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
*
* Clase para pasar datos al formulario de datos de registro
*/
public class NewLaboratoryDatas {
	
	@NotEmpty
	private String laboratoryName;
	@NotEmpty
    private String userName;
	@NotEmpty
	@Size(min=4, max=20)
    private String password;
	@NotEmpty
    private String name;
    private String lastName;
    
    @NotEmpty
    private String email;
    
    public String getLaboratoryName() {
		return laboratoryName;
	}

	public void setLaboratoryName(String laboratoryName) {
		this.laboratoryName = laboratoryName;
	}

    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
