/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uma.web.gui.data;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
/**
 *
 * @author Jose
 * Clase para pasar datos al formulario de datos de registro
 */
public class NewUserDatas {
    
	@NotEmpty
    private String loginName;
    @NotEmpty
	@Size(min=4, max=20)
    private String password;
    @NotEmpty
    private String name;
    private String lastName;
    
    @NotEmpty
    private String email;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
