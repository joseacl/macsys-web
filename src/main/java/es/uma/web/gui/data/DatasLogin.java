package es.uma.web.gui.data;

/**
 * Datos de acceso login (Puede ser admin lab o user lab)
 * @author Jose
 *
 */

public class DatasLogin {
	
	private String labName;
	private String loginName;
	private String password;
	
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	/**
	 * @return the userId
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
