/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uma.web.gui.controller;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.data.NewLaboratoryDatas;
import es.uma.web.gui.data.NewUserDatas;
import es.uma.web.gui.model.Administrator;
import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.model.User;
import es.uma.web.gui.service.LaboratoryService;
import es.uma.web.gui.service.LaboratoryServiceImpl;
import es.uma.web.gui.service.UserService;
import es.uma.web.gui.service.UserServiceImpl;
import es.uma.web.gui.utils.SessionUtils;

/**
 *
 * @author Jose
 * Aqui no hay metodo POST
 */
@Controller
@RequestMapping(value="/access")
public class AdminLoginController {
	
	private static final String ADMIN_VIEW = "adminToolViews/administratorView";
    
    //Pagina de acceso que tiene restricciones registro ADMIN
	@RequestMapping(value="/welcomeAdministrator", method = RequestMethod.GET)
    public String welcome(ModelMap model) {//Miramos si ya se esta logeado
    	updateModelLabsList(model);
    	
		model.addAttribute("registerTab",true);
		
        return ADMIN_VIEW;
    }
	//Metodos de springsecurity
	
	/**
     * Metodo get que devuelve las options de un administrador de laboratorio. Llamado desde spring (Revisar springsecurity-servelt.xml)
     * Puede ser llamado con parametros para pasarlos al jsp (Errores en el formulario o logout)
     * @return 
     */
    @RequestMapping(value="/loginAdminView", method = RequestMethod.GET)
    public String loginAdminView(@RequestParam(value = "error", required = false) String error,
    							 @RequestParam(value = "logout", required = false) String logout,
    							 ModelMap model) {//Miramos si ya se esta logeado
    	//Devolvemos el jsp para logearse un administrador
    	
    	if (error != null) {
			model.put("error", "Invalid username and password!");
		}
 
		if (logout != null) {
			model.put("msg", "You've been logged out successfully.");
		}
    	
        return "LoginAdmin";
    }
    
    /**
     * Metodo get pagina de error en logeado de administrador. Llamado desde spring (Revisar springsecurity-servelt.xml)
     * @return 
     */
    @RequestMapping(value="/newRegister", method = RequestMethod.GET)
	public String register(ModelMap model) {//Miramos si ya se esta logeado
    	updateModelLabsList(model);
		model.addAttribute("registerTab",true);
        return ADMIN_VIEW;
    }
    
    /**
     * Metodo POST que registra un nuevo usuario
     * @param newUserDatas
     * @param result
     * @param model
     * @return 
     */
    @RequestMapping(value = "/registerLaboratory", method = RequestMethod.POST)
    public String regiter(@Valid @ModelAttribute NewLaboratoryDatas newLabDatas, BindingResult result,
		ModelMap model) {
    	model.addAttribute("registerTab",true);
        if (result.hasErrors()) {
            //Validations on client, return page error if any one post a registerLaboratory without validations
        	//Redirect to administratorView
        	return "administratorView";
        } else {
        	String labName = newLabDatas.getLaboratoryName();
        	//En caso de que no haya errores en el formulario. chequear que el nombre del laborarorio y del administrador sean unicos
        	LaboratoryService labService = new LaboratoryServiceImpl();
        	boolean existLab = labService.existLabName(labName);
        	if (existLab) {
        		//return validation error
        		String msg = String.format("Laboratory %s exist in system", labName);
        		List<String> errorList = new LinkedList<String>();
        		errorList.add(msg);
        		model.put("hasErrors", true);
        		model.put("msgList",errorList);
        	}
        	else {
        		//Accedemos al Servicio del laboratorio, el cual ya se encarga de guardar el admnistrador
                
                Laboratory laboratory = new Laboratory();
                laboratory.setLabName(labName);
                
                //Datos del usuario Administrador
                Administrator admin = new Administrator();
                admin.setLoginName(newLabDatas.getUserName());
                admin.setName(newLabDatas.getName()); 
                admin.setPassword(newLabDatas.getPassword());
                admin.setLastName(newLabDatas.getLastName());
                admin.setEmail(newLabDatas.getEmail());
                
                laboratory.setAdmin(admin);
                
                //Al servicio, le indicamos que salve el laboratorio completo
                labService.saveNewLab(laboratory);
        	}
        	
        }
        
        updateModelLabsList(model);

        return ADMIN_VIEW;
    }
    
    
    /**
     * Metodo POST que registra un nuevo usuario
     * @param newUserDatas
     * @param result
     * @param model
     * @return 
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String regiter(@Valid @ModelAttribute NewUserDatas newUserDatas, BindingResult result,
		ModelMap model) {
    	model.addAttribute("registerTab",true);
        if (result.hasErrors()) {
            //Hay errores en los datos rellenados en el formulario newUser
            return "administratorView";
        } else {

            UserService loginService = new UserServiceImpl();
            User userToSave = new User();
            userToSave.setLoginName(newUserDatas.getLoginName());
            userToSave.setPassword(newUserDatas.getPassword());
            userToSave.setName(newUserDatas.getName());
            userToSave.setLastName(newUserDatas.getLastName());

            //Salvamos el usuario recibido desde el form
            loginService.saveNewUser(userToSave);
        }
        
        updateModelLabsList(model);

        return ADMIN_VIEW;
    }

	/**
	 * Called from $(Ajax) in administratorView when push on delete button
	 */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody //Indica que se devuelve un JSON
    public Boolean deleteUser(@RequestParam(value = "labName") String labName,
    		ModelMap model) {
    	//Remove userLoginName
    	LaboratoryService labService = new LaboratoryServiceImpl();
    	boolean result = labService.removeLab(labName);
    	if (result) {
    		updateModelLabsList(model);
    		model.addAttribute("registerTab",false);
    		//Return the only view userList.jsp to render in responde call ajax
    		Laboratory lab = labService.getLabByName(labName);
            return true;
    	}
    	//Si no se elimino correctamente, devolvemos pagina de error
    	return false;
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editUser(@RequestParam(value = "userLogin") String userLoginName) {
    	return ADMIN_VIEW;
    }
    
    /**
	 * Called from $(Ajax) in administratorView when push on delete button
	 */
    @RequestMapping(value = "/labnameAvailable", method = RequestMethod.POST)
    @ResponseBody //Indica que se devuelve un JSON
    public Boolean usernameAvailable(@RequestParam(value = "labname") final String labname,
    		ModelMap model) {
    	LaboratoryService labService = new LaboratoryServiceImpl();
    	boolean existLab = labService.existLabName(labname);
    	if (existLab) {
    		return false;
    	}
    	else {
    		return true;
    	}
    }
    
    private void updateModelLabsList(ModelMap model) {
		//Actualizamos la lista de usuarios
    	LaboratoryService labService = new LaboratoryServiceImpl();
		List<Laboratory> labList = labService.getAllLabs();
		if (labList!=null && !labList.isEmpty()) {
			model.put("hasLabs", true);
			model.put("labsList", labList);
		} else {
			model.put("hasLabs", false);
		}
		
		NewLaboratoryDatas emptyDatas = new NewLaboratoryDatas();
		model.put("", emptyDatas);
	}
    
}
