package es.uma.web.gui.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.data.NewLaboratoryDatas;
import es.uma.web.gui.data.NewUserDatas;
import es.uma.web.gui.model.Administrator;
import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.model.User;
import es.uma.web.gui.service.LaboratoryService;
import es.uma.web.gui.service.LaboratoryServiceImpl;
import es.uma.web.gui.service.UserService;
import es.uma.web.gui.service.UserServiceImpl;
import es.uma.web.gui.utils.ModelUtils;
import es.uma.web.gui.utils.SessionUtils;
import es.uma.web.gui.utils.StringMD;

/**
*
* @author Jose
* Gestion de la creacion (C) , borrado (D) y actulizado (U) de usuarios por parte de administradores de laboratorio
*/
@Controller
@RequestMapping(value="/access")
public class CDUUserController {
	
	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public String regiter(@Valid @ModelAttribute NewUserDatas newUserDatas, BindingResult result,
		ModelMap model) {
		//Check session avaliable
		if (!SessionUtils.userLogged()) {
			ModelUtils.putModelListLabs(model);
			return LoginController.LOGIN_VIEW;
		}
		//Obtenemos el usuario logeado, que en este controlador tiene que ser un administrador de laboratorio
		DatasLogin adminLab = (DatasLogin) SessionUtils.getValueInSession(SessionUtils.USER_LOGIN_DATAS);
		model.put("admin", adminLab);
		model.addAttribute("registerTab",true);
		//De aqui podemos obtener el laboratorio donde estamos trabajando
    	model.addAttribute("registerTab",true);
        if (result.hasErrors()) {
            //Validations on client, return page error if any one post a registerLaboratory without validations
        	//Redirect to administratorView
        	return LoginController.ADMIN_LAB_VIEW;
        } else {
        	//Verificar que el usuario no exista ya en el laboratorio
        	//En caso de que no haya errores en el formulario. chequear que el nombre del laborarorio y del administrador sean unicos
        	String loginName = newUserDatas.getLoginName();
        	String name      = newUserDatas.getName();
        	String lastName  = newUserDatas.getLastName();
        	String email     = newUserDatas.getEmail();
        	String password  = newUserDatas.getPassword();
        	String labName   = adminLab.getLabName();
        	
        	//Obtenemos el laboratorio
        	LaboratoryService labService = new LaboratoryServiceImpl();
        	Laboratory labEntity         = labService.getLabByName(labName);
        	if (labEntity!=null) {
        		User userToSave = new User();
        		userToSave.setLoginName(loginName);
        		userToSave.setName(name);
        		userToSave.setLastName(lastName);
        		userToSave.setEmail(email);
        		userToSave.setPassword(StringMD.getStringMessageDigest(password, StringMD.MD5));
        		userToSave.setLaboratory(labEntity);
            	
            	UserService userService = new UserServiceImpl();
            	boolean existUser       = userService.existUser(userToSave);
            	
            	if (existUser) {
            		//return validation error
            		String msg = String.format("User %s exist in lab %s", loginName, labName);
            		List<String> errorList = new LinkedList<String>();
            		errorList.add(msg);
            		model.put("hasErrors", true);
            		model.put("msgList",errorList);
            	} else {
            		//Salvamos el usuario
            		userService.saveNewUser(userToSave);
            		
            		String msg = String.format("User %s saved sucessfully in lab %s", loginName, labName);
            		List<String> msgList = new LinkedList<String>();
            		msgList.add(msg);
            		model.put("hasMsgs", true);
            		model.put("msgList",msgList);
            		
            	}
        	}
        	
        }
        ModelUtils.putModelListUsers(model);
        return LoginController.ADMIN_LAB_VIEW;
    }
	
	/**
	 * Called from $(Ajax) in administratorView when push on delete button
	 */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    @ResponseBody //Indica que se devuelve un JSON
    public Boolean deleteUser(@RequestParam(value = "loginName") final String loginName,
    		ModelMap model) {
    	
    	if (!SessionUtils.userLogged()) {
			return false;
		}
    	
    	//Obtenemos el nombre del laboratorio del usuario administrador logeado en la sesion
    	DatasLogin userLogin = (DatasLogin) SessionUtils.getValueInSession(SessionUtils.USER_LOGIN_DATAS);
    	String labName       = userLogin.getLabName();
    	LaboratoryService labService = new LaboratoryServiceImpl();
    	Laboratory laboratory = labService.getLabByName(labName);
    	if (laboratory!=null) {
    		Set<User> userOfLab = laboratory.getUsers();
    		//Obtenemos el usuario concreto a borrar
    		//Usamos guava funtions, definimos predica que filtra por loginName
    		Predicate<User> userFilterPredicate = new Predicate<User>() {
    			  public boolean apply(User user) {
    				  return user.getLoginName().equals(loginName);
    			  }
    			};
    		Collection<User> userFilter = Collections2.filter(userOfLab, userFilterPredicate);
    		List<User> usersToDelete = new LinkedList<User>(userFilter);
    		if (usersToDelete.size() == 1) {
    			User userToDelete = usersToDelete.get(0);
    			UserService userService = new UserServiceImpl();
    			boolean result = userService.delete(userToDelete);
    			return result;
    		}
    	}
    	//Si no se elimino correctamente, devolvemos pagina de error
    	return false;
    }
}
