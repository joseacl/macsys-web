package es.uma.web.gui.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import es.uma.web.gui.data.NewUserDatas;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.uma.web.gui.data.DatasLogin;
import es.uma.web.gui.model.Laboratory;
import es.uma.web.gui.model.User;
import es.uma.web.gui.service.AdminLabService;
import es.uma.web.gui.service.AdminLabServiceImpl;
import es.uma.web.gui.service.LaboratoryService;
import es.uma.web.gui.service.LaboratoryServiceImpl;
import es.uma.web.gui.service.UserService;
import es.uma.web.gui.service.UserServiceImpl;
import es.uma.web.gui.utils.ModelUtils;
import es.uma.web.gui.utils.SessionUtils;
import es.uma.web.gui.utils.StringMD;

import javax.validation.Valid;

import org.springframework.validation.BindingResult;

@Controller
@RequestMapping(value="/access")
/**
 * @author Jose
 * 
 * Clase encargada de modelar el acceso al sistema, ya sean administradores de laboratorio o usuarios propios del laboratorio
 *
 */
public class LoginController {

	public static final String ADMIN_LAB_VIEW = "adminLabViews/adminLabView";
	public static final String USER_LAB_VIEW  = "userLabViews/userLabView";
	
	//Vista de login
	public static final String LOGIN_VIEW     = "loginView";
	
	/**
	 * Metodo POST que hace login de un usuario
	 * @param userLogin
	 * @param model
	 * @return 
	 */
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String login(@ModelAttribute("userLogin") DatasLogin userLogin,
		ModelMap model) {
	
		ModelUtils.putModelListLabs(model);
		
		String loginName = userLogin.getLoginName();
		String labName   = userLogin.getLabName();
        AdminLabService adminService = new AdminLabServiceImpl();
        boolean isAdmin = adminService.verifyLogin(userLogin);
        if (isAdmin) {
        	SessionUtils.saveValueInSession(SessionUtils.USER_LOGIN_DATAS, userLogin);
        	SessionUtils.saveValueInSession(SessionUtils.ADMIN_LAB_FLAGS, true);
        	model.put("admin", userLogin);
        	model.addAttribute("registerTab",true);
        	//Establecemos la lista de usuarios
        	ModelUtils.putModelListUsers(model);
            return ADMIN_LAB_VIEW;
            //SessionUtils.saveValueInSession(SessionUtils.USER_DATA_BASE, userInDb);
        }
        
        else {
	        //Si no es admin, verificamos si es usuario
	    	String password  = userLogin.getPassword();
	    	LaboratoryService labService = new LaboratoryServiceImpl();
	    	Laboratory labEntity         = labService.getLabByName(labName);
	    	
	    	if (labEntity!=null) {
	    		UserService userService = new UserServiceImpl();
	    		User userToCheck = new User();
	    		userToCheck.setLoginName(loginName);
	    		userToCheck.setPassword(StringMD.getStringMessageDigest(password, StringMD.MD5));
	    		userToCheck.setLaboratory(labEntity);
	            if (userService.verifyLogin(userToCheck)) {
	                //Obtenemos los datos del usuario, para guardarlo en la sesion
	                //Obtenemos el usuario salvado en base de datos, a partir del loginName
	                User userInDb = userService.getUserByLoginName(userLogin.getLoginName());
	                SessionUtils.saveValueInSession(SessionUtils.USER_LOGIN_DATAS, userLogin);
	                SessionUtils.saveValueInSession(SessionUtils.ADMIN_LAB_FLAGS, false);
	                model.put("user", userLogin);
	                return USER_LAB_VIEW;
	            } else {
	            	//Mostramos error, usuario o administrador no encontrado en el sistema
	            	String msg = String.format("loginName %s does not exist in system", loginName);
	        		List<String> errorList = new LinkedList<String>();
	        		errorList.add(msg);
	        		model.put("hasErrors", true);
	        		model.put("msgList",errorList);
	        		
	        		//En la vista loginView se muestra un box con los mensajes de error
	                return LOGIN_VIEW;
	            }
	    	} else {
	    		//Pagina de error
	    		return "error";
	    	}
        }
	}
        
        
	
    /**
     * Metodo get que devuelve el jsp de la vista de usuario
     * @return 
     */
	@RequestMapping(value="/welcome", method = RequestMethod.GET)
	public String welcome(ModelMap model) {//Miramos si ya se esta logeado
		ModelUtils.putModelListLabs(model);
        if (SessionUtils.userLogged()) {
        	//Determinar si es usuario o administrador
        	boolean isAdmin = (Boolean) SessionUtils.getValueInSession(SessionUtils.ADMIN_LAB_FLAGS);
        	DatasLogin userLogin = (DatasLogin) SessionUtils.getValueInSession(SessionUtils.USER_LOGIN_DATAS);
    		
    		model.addAttribute("registerTab",true);
        	if (isAdmin) {
        		model.put("admin", userLogin);
        		ModelUtils.putModelListUsers(model);
        		return ADMIN_LAB_VIEW;
        	} else {
        		model.put("user", userLogin);
        		return USER_LAB_VIEW;
        	}
        }
        else {
            return LOGIN_VIEW;
        }
	}
        
        
	@RequestMapping(value="/logoutOfLab", method = RequestMethod.GET)
	public String logout(ModelMap model) {
        SessionUtils.logoutOfSession();
        
        ModelUtils.putModelListLabs(model);
        
        return LOGIN_VIEW;
	}
	
	/**
	 * Called from $(Ajax) in administratorView when push on delete button
	 */
    @RequestMapping(value = "/usernameAvailable", method = RequestMethod.POST)
    @ResponseBody //Indica que se devuelve un JSON
    public Boolean usernameAvailable(@RequestParam(value = "username") final String username,
    		ModelMap model) {
    	DatasLogin adminLab = (DatasLogin) SessionUtils.getValueInSession(SessionUtils.USER_LOGIN_DATAS);
    	String labName   = adminLab.getLabName();
    	
    	//Obtenemos el laboratorio
    	LaboratoryService labService = new LaboratoryServiceImpl();
    	Laboratory labEntity         = labService.getLabByName(labName);
    	if (labEntity!=null) {
    		User userToSave = new User();
    		userToSave.setLoginName(username);
    		userToSave.setLaboratory(labEntity);
        	
        	UserService userService = new UserServiceImpl();
        	boolean existUser       = userService.existUser(userToSave);
        	
        	if (existUser) {
        		return false;
        	} else {
        		return true;
        	}
    	}
    	return false;
    }
        
	
}
