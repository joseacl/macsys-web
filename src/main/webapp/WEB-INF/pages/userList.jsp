<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<!-- Check if model return user list -->
<c:choose>
<c:when test="${hasUsers}">
<table id="userTable">
	<tr>
		<th> Username</th>
		<th> Name</th>
	</tr>
	<!-- Iterate for the user list to build the list with options -->
	<c:forEach var="user" items="${userList}">
	<tr>
	<td>
		<c:out value="${user.loginName}"></c:out>
	</td>
	<td>		                			
		<c:out value="${user.name}"></c:out>
	</td>
			<!-- Options. Edit or Delete buttons -->
		<td>
			<a href="#" class="confirm" id="delete${user.loginName}" title="Are you sure you want to delete the user ${user.name}?">
				<img src="<%= request.getContextPath()%>/resources/css/images/deleteButton.png"
					width="24" height="24" alt="submit"/>
			</a>
			<a href="#" class="confirm" id="edit${user.loginName}" title="Are you sure you want to edit the user ${user.name}?" >
				<img src="<%= request.getContextPath()%>/resources/css/images/edit.png"
					width="24" height="24" alt="submit"/>
			</a>									
		</td>
	</tr>
	</c:forEach>
</table>
</c:when>
<c:otherwise >
	<c:out value="Theres is not users in system ..."></c:out>
</c:otherwise>
</c:choose>
</body>
</html>