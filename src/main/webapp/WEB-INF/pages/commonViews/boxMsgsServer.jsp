<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	<!-- The overlay and the box to show server validations -->
	<!-- if there is server errors validations, show the box on top, style=top: 160px -->
	<!-- else, do not show the box -->
<script src="<c:url value="/resources/js/box_function.js" />"></script>
<link href="<c:url value="/resources/css/boxStyle.css" />" rel="stylesheet">

<div class="overlay" id="overlay" style="display:none;"></div>
<c:choose>
<c:when test="${hasMsgs}">
	<div class="boxInfo" id="box" style="top: 160px">
</c:when>
 <c:otherwise>
 	<div class="boxInfo" id="box" style="top: -200px">
 </c:otherwise>
 </c:choose>
    <a class="boxclose" id="boxclose"></a>
    <h1>Information msgs</h1>
    <c:forEach var="msg" items="${msgList}">
   		<p> ${msg} </p>
       </c:forEach>
</div>
	

