<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acceso al sistema</title>
<link href="<c:url value="/resources/css/loginStyle.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/generalStyles.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script src="<c:url value="/resources/js/index.js" />"></script>
</head>
<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}
 
.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}
 
#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>
<body>
    <div class="login-card">
        <h1>Administrator Login</h1><br>
        <div class="flat-form">
            <ul class="tabs">
                <li>
                    <a href="#login" class="active">Login</a>
                </li>
            </ul>
            
            <c:if test="${not empty error}">
                <div class="error">${error}</div>
            </c:if>
            <c:if test="${not empty msg}">
                <div class="msg">${msg}</div>
            </c:if>
	    	
            <div id="loginForm" class="form-action show">
                <form method="post" action="<c:url value='../j_spring_security_check' />">
                    <table>
                        <tr>
                            <td><label for="username">Administrator Name</label></td>
                            <td><input type="text" name="j_username"/></td>
                        </tr>
                        <tr>
                            <td><label for="password">Password</label></td>
                            <td><input type="password" name="j_password"/></td>
                        </tr>

                        <tr>
                            <td colspan="3"> <input text="Log in" type="submit" class="btn"></input> </td>
                        </tr>
					</table>
					
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>