<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="<c:url value="/resources/css/generalStyles.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/headerStyle.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>

<title>Options Page</title>
</head>
<body>

    <h1> Welcome ${user.loginName} to system <h1>
    <a href="${pageContext.request.contextPath}/access/logoutOfLab">Logout</a> 
</body>
</html>