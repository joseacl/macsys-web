<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error Page not found</title>
   <header class="header  header--large">
		<h1 class="title">Ahhhhhhhhhhh! This page doesn't exist ERROR 405</h1>
		<h2 class="strapline">Not to worry. You can either head back to 
			<a href="${pageContext.request.contextPath}/access/welcome">our homepage</a>, or sit there and listen to a goat scream like a human.</h2>
	</header>
	
	<div class="content  fit-vid  vid">
		<iframe src="http://www.youtube.com/embed/SIaFtAKnqBU?vq=hd720&amp;rel=0&amp;showinfo=0&amp;controls=0&amp;iv_load_policy=3&amp;loop=1&amp;playlist=SIaFtAKnqBU&amp;modestbranding=1&amp;autoplay=1" width="560" height="315" frameborder="0" webkitallowfullscreen="" allowfullscreen=""></iframe>
	</div>
</html>