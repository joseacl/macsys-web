<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acceso al sistema</title>
<link href="<c:url value="/resources/css/loginStyle.css" />" rel="stylesheet"> 
<link href="<c:url value="/resources/css/generalStyles.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/tablesStyle.css" />" rel="stylesheet">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/blitzer/jquery-ui.css" type="text/css" />
<script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
<script src="<c:url value="/resources/js/index.js" />"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<c:url value="/resources/js/jquery.easy-confirm-dialog.js" />"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		//when button is clicked  
        $('#check_labname_availability').click(function(){  
            check_availability();  
        }); 
		//function to check username availability  
		function check_availability(){  
		  
	        //get the username  
	        var labnameInput = $('#laboratoryNameId').val();  
	        $.ajax({
	             type : "POST",
	             url : "${pageContext.request.contextPath}/access/labnameAvailable",
	             data: { labname: labnameInput } ,
	             success : function(response) {
	            	 //if respose = false
	            	 if(response){  
	                    //show that the username is available  
	                    $('#labname_availability_result').html(labnameInput + ' is Available');  
	                 }else{  
	                    //show that the username is NOT available  
	                    $('#labname_availability_result').html(labnameInput + ' is not Available');  
	                 } 
	             }
	        });
	        
		} 
		
		$(".confirm").easyconfirm();
    	<c:forEach var="lab" items="${labsList}">
			$("#delete${lab.labName}").click(function() {
				$.ajax({
		             type : "POST",
		             url : "${pageContext.request.contextPath}/access/delete",
		             data: { labName: "${lab.labName}" } ,
		             success : function(response) {
		            	 //if respose = false
		            	 if (!response) {
		            		 alert("Lab ${lab.labName} not delete");
		            	 } else {
		            		//Borramos de la tabla la fila (ID) correspondiente la laboratorio
			            	$('table#labTable tr#${lab.labName}').remove();	 
		            	 }
		             },
		             error: function(e){  
	                 	alert('Error: ' + e);  
	                 }  
		        });
			});
			
			$("#edit${lab.labName}").click(function() {
				alert("You approved the action edit ");
			});
		
		</c:forEach>
		
		//Validacion de formularion
		// validate signup form on keyup and submit on client
		$("#signupForm").validate({
			rules: {
				laboratoryName: "required",
				name: "required",
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 4
				},
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				laboratoryName: "Please enter the laboratory name",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				email: "Please enter a valid email address",
			}
		});
		
	});
	
	
</script>
</head>
<body>
	<%@include file="../commonViews/boxValidationsServer.jsp" %>
    <div class="login-card">
     	<header class="signup-header">
	    	<h1 class="signup-logo"></h1>
	        <h2>Create laboratory</h2>
        </header>
        <div class="flat-form">
            <ul class="tabs">
                <li>
                    <a href="#register">Register</a>
                </li>
                <li>
                    <a href="#labList">Laboratory List</a>
                </li>
            </ul>
    		<!-- Regiter Tab -->
    		<!-- check atributte registerTab to show that tab -->
    		<c:if test="${registerTab}" >
    			<div id="register" class="form-action show" style="display: block">
    		</c:if>
            <c:if test="${!registerTab}" >
    			<div id="register" class="form-action show" style="display: none">
    		</c:if>
               <form id="signupForm" method="post" action="<%= request.getContextPath() %>/access/registerLaboratory" >
               <table>
               		<tr>
	                    <td>Laboratory Name :</td>
	                    <td><input id="laboratoryNameId" name="laboratoryName"/></td>
	                    <td><input type='button' id='check_labname_availability' value='Check Availability'></td>  
						<td><div id='labname_availability_result'></div></td>  
					</tr>
					<tr>
	                    <td>Administrator :</td>
	                    <td><input id="userNameId" name="userName"/></td>
					</tr>
					<tr>
	                    <td>Password :</td>
	                    <td><input id="passwordId" type="password" name="password"/></td>
					</tr>
                    <tr>
                        <td>Name :</td>
                        <td><input id="nameId" name="name" /></td>
					</tr>
                    <tr>
	                    <td>Lastname :</td>
	                    <td><input id="lastNameId" name="lastName" /></td>
					</tr>
					<tr>
	                    <td>Email :</td>
	                    <td><input id="emailId" name="email" /></td>
					</tr>
					<tr>
                        <td colspan="3"><input type="submit" class="btn" /></td>
					</tr>
             </table>
             </form>
                
                
	            <!-- csrt for log out-->
	    		<c:url value="/j_spring_security_logout" var="logoutUrl" />
				<form action="${logoutUrl}" method="post" id="logoutForm">
				  <input type="hidden" 
					name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				</form>
				<a href="javascript:formSubmit()"> Logout</a>
				<script>
					function formSubmit() {
						document.getElementById("logoutForm").submit();
					}
				</script>
        </div>
        
        <!-- User List Tab -->
        <c:if test="${registerTab}" >
        	<div id="labList" class="form-action hide" style="display: none">
   		</c:if>
           <c:if test="${!registerTab}" >
   			<div id="labList" class="form-action hide" style="display: block">
   		</c:if>
        
             <!-- Check if model return user list -->
             <c:choose>
               	<c:when test="${hasLabs}">
	                <table id=labTable class="tableList">
	                	<tr>
	                		<th> Lab name</th>
	                		<th> Admin login</th>
	                		<th> Admin Name</th>
                		</tr>
	                	<!-- Iterate for the user list to build the list with options -->
	                	<c:forEach var="lab" items="${labsList}">
		                	<tr id="${lab.labName}">
		                		<td>
 		                			<c:out value="${lab.labName}"></c:out>
	                			</td>
		                		<td>		                			
 		                			<c:out value="${lab.admin.loginName}"></c:out>
	                			</td>
	                			<td>		                			
 		                			<c:out value="${lab.admin.name}"></c:out>
	                			</td>
	                			<!-- Options. Edit or Delete buttons -->
	                			<td>
									<a href="#" class="confirm" id="delete${lab.labName}" title="Are you sure you want to delete the laboratory ${lab.labName}?">
										<img src="<%= request.getContextPath()%>/resources/css/images/deleteButton.png"
	 	 		                				width="24" height="24" alt="submit"/>
									</a>
									<a href="#" class="confirm" id="edit${lab.labName}" title="Are you sure you want to edit the laboratory ${lab.labName}?" >
										<img src="<%= request.getContextPath()%>/resources/css/images/edit.png"
	 	 		                				width="24" height="24" alt="submit"/>
									</a>
									
									
	                			</td>
                			</tr>
	                	</c:forEach>
                   </table>
	            </c:when>
	            <c:otherwise >
	                <c:out value="Theres is not users in system ..."></c:out>
	            </c:otherwise>
            </c:choose>
    </div>    
</body>
</html>
