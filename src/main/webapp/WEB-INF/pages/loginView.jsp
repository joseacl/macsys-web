<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Access</title>
<link href="<c:url value="/resources/css/loginStyle.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/generalStyles.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery.validate.js" />"></script>
<!-- Includes to css and js to show box or pop up to important messages -->
<script src="<c:url value="/resources/js/box_function.js" />"></script>
<link href="<c:url value="/resources/css/boxStyle.css" />" rel="stylesheet">
<script type="text/javascript">
	$(document).ready(function() {
		$("#signupForm").validate({
			rules: {
				loginName: "required",
				password: "required",
				labName : "required"
			},
			messages: {
				loginName: "Please enter your login name",
				password: "Please enter your password",
				labName : "Plesse select a laboratory"
			}
		});
	});
</script>
</head>
<body>
	<!-- Posibles errores lanzados desde el servidor, tratados para todas las paginas en boxValidationServer.jsp -->
	<%@include file="commonViews/boxValidationsServer.jsp" %>
    <div class="login-card">
        <h1>Login</h1><br>
        <div class="flat-form">
            <ul class="tabs">
                <li>
                    <a href="#login" class="active">Login</a>
                </li>
            </ul>
	    	
            <div id="login" class="form-action show">
                <form id="signupForm" method="post" modelAttribute="userLogin" action="<%= request.getContextPath() %>/access/login">
                <table>
                    <tr>
                        <td><label for="username">Username</label></td>
                        <td><input type="text" id="loginName" name="loginName"/></td>
                    </tr>
                    <tr>
                        <td><label for="password">Password</label></td>
                        <td><input type="password" id="password" name="password"/></td>
                    </tr>
                    <!-- Lista de laboratorios, para que el usuario indique al que pertenece -->
                    <!-- Iterate for the user list to build the list with options -->
                    <tr>
                    	<td>
	                    	<select name="labName">
			                	<c:forEach var="lab" items="${labsList}">
			                		<option value="${lab.labName}">${lab.labName}</option>
			                	</c:forEach>
	                		</select>
                		</td>
               		</tr>
                    <tr>
                        <td colspan="3"> <input text="Log in" type="submit" class="btn"></input> </td>
                    </tr>
                </form>
            </div>
        </div>
    </div>
</body>
</html>